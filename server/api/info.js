const express = require('express');
const router = express.Router();
const { spawn } = require('child_process');


// @route         GET node/info
// @description   get node info
// @access        Public

router.get('/', (request, response) => {

    const getinfo = spawn('coind', ['getinfo']);

    getinfo.stdout.on('data', (data) => {
        let resData = `${data}`;
        let json = JSON.parse(resData);
        response.json({
            "connectPeers": json.connections,
            "height": json.syncblock_height,
            "gasPrice": null
        });

        getinfo.stderr.on('data', (data) => {
            response.json(JSON.parse(`${data}`));
            console.error(`stderr: ${data}`);
        });
    });
});

module.exports = router;
