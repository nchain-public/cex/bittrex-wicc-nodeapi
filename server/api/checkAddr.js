const express = require('express');
const router = express.Router();
const { spawn } = require('child_process');

// @route         GET node/address/:addr
// @description   get node address information with an address
// @access        Public

router.get('/:addr', (request, response) => {

    let address = request.params.addr;

    const getaccountinfo = spawn('coind', ['getaccountinfo', address]);

    getaccountinfo.stdout.on('data', (data) => {

        let addrJson = JSON.parse(`${data}`);
        let balance = addrJson.tokens.WICC.free_amount;

        if ( balance !== 0.0001) {
            response.json(
                {
                    "balances": [
                        {
                            "asset": "WICC",
                            "balance": balance
                        },
                    ],
                    "nonce": null
                })
        } else {
            response.json(
                {
                    "balances": [
                        {
                            "asset": "WICC",
                            "balance": 0
                        },
                    ],
                    "nonce": null
                })
        }
    });

    getaccountinfo.stderr.on('data', (data) => {

        response.status(404).json(`${data}`)
    });

});


module.exports = router;
